# A learning portfolio based on CAT true scores and Complex Learning IDM


## Abstract
<!-- will write at the end --> 



## Introduction
The widespread use of item response theory in educational assessment is usually justified by its ability to generate true scores, or scores that are equal to the students' knowledge level. While true scores are inherently valuable in providing students an assessment of their individual skill levels, the representation of whole learning tasks, to the best of our knowledge complex skills with multiple constructs and different proficiency levels has not been explored.  

<!-- lit review on learning tasks - check references from book Merriëboer -->

<!-- lit review on graphical methods to display progression of latent constructs over time -->

The objective of this article is therefore to compare different skill portfolio representations using a mixed methods methodology, and also presenting an application that allows for integration of this display into Web frameworks within learning management systems. 



## Methods

### Data simulation

In order to conduct this experiment, we simulated a series of variables over time for the following Complex Learning constructs: learning tasks, sequencing, ability to find supporting info, procedural info. Table 1 presents an operational definition for each of these constructs. 


Construct | Definition
----------|-----------
Learning tasks |  hi
Sequencing | 
Ability to find supporting information |
Procedural information | 


* [simFrame](http://cran.r-project.org/web/packages/simFrame/vignettes/simFrame-intro.pdf)
* [simPopulation](http://cran.r-project.org/web/packages/simPopulation/vignettes/simPopulation-eusilc.pdf)

### Portfolio components

* each construct measured during course
* true scores
* progression over time following complex learning sequence: learning tasks, sequencing, ability to find supporting info, procedural info
* comparison with other students



### Presentation
* widget on home page for LMS
* ability to share with specific people, groups, or publicly on social networks (email, twitter, g+, fb, linkedin)




### First iteration
* simulate data for 2 whole learning tasks, 3 sequencing levels, 3 ability to find supporting info, 3 procedural info
* generate graphic ggplot2
* transform into shiny app
* integrate into django


* UX for different displays
* conjoint analysis interviews on hangout using RDQA

<!-- check graphic on XS for other methods -->




## Results

## Discussion

### Primacy and summary

### Result 1
### Result 2
### Result 3
### Result 4
### Limitations

### Future










come up with a strategy to calibrate each one of our tests against some known widely accepted metric. since we are niche hacking, more often than not **we will become the standard**, which then aligns with our strategy to also have standardized tests outside of training
design the initial, ultra-simplified version of what this IRT/CAT-based portfolio would look like. won't go into details here, but since our IRT/CAT is based on R we could explore shiny http://goo.gl/etgRD, animations http://goo.gl/fwvDI , rcharts http://goo.gl/OdWyT since it creates an interface with js, and then throw this into the student home page Bruno created as well as create an interface with social networks (fb/linkedin/g+) to be shown to their VIN very important network whenever they might want to
create a mechanism to communicate this value to students so that they can understand it and prefer our courses over others
