library("mirt")
head(LSAT7)
data <- expand.table(LSAT7)
head(data)
(mod1 <- mirt(data, 1)) # one factor
coef(mod1)
summary(mod1)
residuals(mod1)
residuals(mod1, restype = "exp")
(mod2 <- mirt(data, 2))
summary(mod2, rotate = "oblimin", suppress = 0.25)
anova(mod1, mod2)

data("SAT12")
head(SAT12)
data <- key2binary(SAT12, key = c(1, 4, 5, 2, 3, 1, 2, 1, 3, 1, 2, 4, 2, 1, 5, 3, 4, 4, 1, 4, 3, 3, 4, 1, 3, 5, 1, 3, 1, 5, 4, 5)) # turning data into binary format
head(data)
specific <- c(2, 3, 2, 3, 3, 2, 1, 2, 1, 1, 1, 3, 1, 3, 1, 2, 1, 1, 3, 3, 1, 1, 3, 1, 3, 3, 1, 3, 2, 3, 1, 2) # determines which factor corresponds to each item 
guess <- rep(0.1, 32) # lower asymptote is fixed for all items
(b_mod1 <- bfactor(data, specific)) # had to take the guess parameter as it wasn't running
coef(b_mod1)