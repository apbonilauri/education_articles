# Skill needs among biomedical researchers in Brazil: Data enriched, dynamic reproducible results

## Abstract
<!-- will write at the end --> 

## Introduction



## Methods

### Target population, sample frame


### IRB (Institutional Review Board) 
approval and informed consent process IRB approval
Describe the informed consent process
Data protection

### Development and pre-testing development and testing


### Recruitment process and description of the sample having access to the questionnaire
Contact mode
Survey administration Web/E-mail


### Context
voluntary
Incentives


### Randomization of items or questionnaire
Adaptive questioning
Number of Items
Number of screens (pages)
Completeness check
Review step

### Data enrichment
<!-- here will use the same methodology used in Elias' paper -->

### Reproducible research protocol

As partially described in previous sections, we followed a reproducible research protocol along the lines previously described by our group [(Vissoci, 2013)](). Briefly, all data sets, scripts, templates, software and workflows generated under this project were deposited under the public repositories [Github]() and [figshare](). For access to the specific data for this project please refer to 
<!-- please add reference once available -->

reproducible research - data and .rmd, shiny, css, js, irt display from g+ repository, zotero library, concerto files

[Item analysis application](https://github.com/EconometricsBySimulation/2013-05-29-ShinyApp) by Francis Smart



## Results

## Discussion

### Primacy and summary

### Result 1
### Result 2
### Result 3
### Result 4
### Limitations

### Future



<!-- criar documentos publicos de supporting e procedural info (merrienboer) como base para cursos mais imersivos futuros -->