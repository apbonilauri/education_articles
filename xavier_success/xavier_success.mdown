Xavier Success: Situated, multimedia, multidimensional item bank and computerized adaptive testing to measure student success drivers in a learning environment using a reproducible research methodology


Joao Vissoci
Elias Carvalho
Adelia Batilana
Ricardo Pietrobon


## Abstract
<!-- will write at the end --> 

## Introduction

One of the central tenets of adult education is that it should be focused on learning that is meaningful to the individual student rather than simply provide learning for the sake of learning. 
<!-- ref from andragogy kindle books --> While this principle is attempted in most curricula, it is rarely the case that a course will change its structure to adapt to the individual student definitions of what would make it a personal success. While multiple scales have being validated to identify what exactly constitutes success to an individual student, to our knowledge no previous measurement tools have incorporated the concept of success being a situated or contextual concept, allowing it to be deployed in an online environment, and measuring the multiple dimensions of success in an adaptive manner.

<!-- Elias and Adelia, please throw in here the link to the zotero group about success measurement -->

<!-- Elias and Adelia, please throw in here the zotero link for the literature on the situatedness of success -->

In face of this gap in the literature, the objective of this study is to present a situated, multimedia, multidimensional item bank and corresponding computerized adaptive test to measure student success drivers in a learning environment. Our study adheres to a reproducible research methodology in order to improve its methodological quality.

## Methods
<!-- summary goes here -->

<!-- create div tag for item bank/CAT: situated, multimedia, multidimensional   -->


### Reproducible research methodology

As partially described in previous sections, we followed a reproducible research protocol along the lines previously described by our group [(Vissoci, 2013)](). Briefly, all data sets, scripts, templates, software and workflows generated under this project were deposited under the public repositories [Github]() and [figshare](). For access to the specific data for this project please refer to 
<!-- please add reference once available  -->

## Results

## Discussion

### Primacy and summary

### Result 1
### Result 2
### Result 3
### Result 4
### Limitations

### Future

## Appendix

<!-- Elias and Adelia, please add scale items here along with their scale name and respective constructs -->



http://goo.gl/g4yc0


ere's my take on this document. I have included questions to the right of the original text in [ ]  I find these questions lacking.


 Content & Variables of the Personal Success Questionnaire 
Demographic Questions, etc. 
1. 
What is your age ? (quantitative) OK

Circle your gender (2 categories) [2 categories? Really? Hope we are not going to fall into that one!]

Circle your ethnic/racial membership (6 categories) [Is there no way with modern technology we can allow people to say who they really are racially? 6 cateogories does not make it and creates natural resentment.]

Circle the type of family in which you were raised (4 categories)  [What are the types? (single mother? Single father? Mother and father present? Lesbian parents? Gay male parents? With siblings? With no siblings?]

Approximately what was your families’ income during your senior year in high school? (quantitative) [MISSPELLING:  family's. Bet a lot of people don't know this. Wouldn't it be smarter to ask their perception? Like Economically, did you feel that your family was 1) poor 2) struggling, but okay 3) I always had everything I needed 4) I had more than others 5) I was brought up very wealthy.]

How many years of formal education did you mother attend? (quantitative) [would rather split this into elementary, secondary and higher education. How you interpret "formal" depends upon where you are from.]

How many years of formal education did your father attend? (quantitative)  [see above]

How many siblings do you have ? (quantitative) [okay, but should be above with type of family]

How many times did you move as a child ? (quantitative)[ edit sentence ...times did your household move when you were a child.]

What was the population of your hometown ? (quantitative) [Again, who knows the answer to this? How about 1) I lived in the country with few neighbors 2) small town 3) large town 4) city]

What is your current GPA (quantitative) [okay]

What is the average number of credit hours that you take per semester? (quantitative) [okay]

During the school year, how many hours do you work at a job each week? (quantitative) [okay]

During the school year, how many hours do you study each week? (quantitative) [okay, though I bet no one really knows since study is on the go now, which is what we're hoping for too! Study as a part of life, not a separate activity.]

Circle your current plans for after college? (3 categories)  [SO curious what the limited 3 categories might be?!]﻿