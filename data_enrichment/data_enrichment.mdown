# Data enrichment for educational market research surveys through crowded data collection and public data sources: A reproducible research project 


Elias Carvalho
Jose Eduardo Santana
Jacson Barros
Joao Vissoci
Adelia Batilana
Ricardo Pietrobon

<!-- A biblioteca do R que usamos no HPV chamada GeoPlot também busca dados do IP, portanto não precisarei rodar o Python dentro do R. Sendo assim, podemos fazer todo o trabalho no R. -->

<!-- acho que ate da pra plotar isso num scatter plot - teoricamente isso nao esta correto porque zip code nao e um numero continuo e sim de localizacao geo-espacial, mas na pratica os numeros sao tao proximos http://goo.gl/fDITe que acho que pode ate dar certo. -->


<!-- freedbase http://goo.gl/rJiUs -->


<!-- Somente para auxiliar a sua documentação, vou utilizar somente o R. Como netse momento só vou plotar graficamente, não precisarei usar o pyhton.

Seguem os packages que vou utilizar:

GeoPlot ( http://goo.gl/3SEIx )
googleVIs ( http://goo.gl/p31xK ) 
RMongo ( http://goo.gl/JJQsp )

eu poderia utilizar o rmongodb ( http://goo.gl/QsO75 ), mas sua utilização não é tão friendly como o RMongo. Por este motivo não optei por ele.


PS: Até este momento, fiz o seguinte:

Criei uma collection no meu Mongo DB e importei sua planilha.. já conectei com o R e funciona muito bem. Vou agora gerar o "fetch" na base de dados e plotar os IPs. Com o GeoPlot eu consigo obter as coordenadas e posso utilizar o GoogleVis para obter os recursos gráficos do Google. Espero até domingo estar com isto pronto.
 -->



<!-- Elias please check  knitcitations http://goo.gl/07EHJ  -->
<!-- indexing http://statcompute.wordpress.com/2013/06/09/improve-the-efficiency-in-joining-data-with-index/ -->

<!-- How to detect three types of hidden data, to eliminate opportunity costs http://www.datasciencecentral.com/profiles/blogs/big-data-not-big-enough-the-cost-of-hidden-data -->

<!-- facebook API http://goo.gl/UUeMZ - use cases and NLTK -->
<!-- select bank for enrichment through geolocation -->
<!-- ggmap -->

<!-- pessoal, li algumas vezes a documentacao da api do facebook http://goo.gl/kHveB e me parece que entre os objetos ali os mais importantes vao ser 

user http://goo.gl/pBo9S - aqui e que esta a maior parte das informacoes interessantes
post http://goo.gl/1XEuk - isso daqui so pode ser processado com muita NLP, mas e a mina das atividades da pessoa. aqui teria de dar uma olhada nos dados pra ver o que vem mas potencialmente poderia combinar com a friendlist pra ver quem sao as pessoas que mais interagem com a pessoa pra criar uma weighted rede, ou seja, se no ultimo mes eu like ou comentei nos posts do jose eduardo um numero x de vezes esse e o peso da minha relacao com ele
friendlist http://goo.gl/BRFgy - isso daqui seria interessante mais tarde pra montar uma rede social da pessoa
essas coisas acima e o que eu consigo imaginar, mas teria de puxar os dados pra ver se e realmente assim que funciona. 

outra coisa, ontem eu recebi dados (anexo) que tem o endereco real das pessoas do outro banco que eu te mandei, e entao isso poderia ser usado pra verificar se os ips batem ou nao com o endereco real -->

## Abstract
<!-- will write at the end --> 

## Introduction
Contemporary education is largely based on untested presumptions about societal demand, rarely if ever submitted to a formal market research to determine what is actually needed. This somewhat of disconnect from market needs can be connected to for a series of events, from the current crisis in the humanities courses at the universities all the way to a large percentage of online courses <!-- need to find actual numbers --> not having an audience or having a very short lifespan. Market research is mostly absent from education probably not because of a lack of interest, but primarily because the tools and costs associated with these procedures are not widely accessible to educational institutions.

While causal attributions cannot be directly made, examples pointing to a lack of connection between our current educational content and students/market needs abound. For example, even with the MOOC (Massive Open Online Courses) (McAuley, 2010;Pappano, 2012) bringing in a very large number of students, attrition rates can be over 95% (Jordan, 2013) 
<!-- add reference, search my g+ DONE -->. One could argue that one of the important factors of such a massive dropout rate is the lack of relevance of the course content to the professional life of most students. While MOOCs bring attrition rates to an extreme, attrition rates within regular university and college programs are now larger than ever before, likely because many now question the relevance of a high education degree to their professional lives. 
<!-- references google plus -->. In contemporary Europe this is certainly more true now than ever before. 
<!-- need reference talking about economic crisis in Europe and employment rates after a degree in the humanities -->

<!-- Elias and Adelia, please add references into Zotero - can be from Google or other places --> 

The disconnect is common knowledge, not know to just a few specialists, which then begs the question of why these educational institutions are not being more proactive in aligning their offerings to real and current market needs. While there are certainly historical reasons for universities to position themselves as "ivory towers," 
<!-- need a book reference here --> much of the disconnect comes from the perceived difficulty and expense incurred in attempting to create market research surveys. These surveys, while useful, require databases containing a substantial amount of information from respondents. Since providing information requires time, respondents have to be compensated at a corresponding level, ultimately bringing the survey cost to a level that is not affordable to most educational institutions. As a result, the disconnect persists, ultimately perpetuating the problem.

<!-- Elias and Adelia, please add references into Zotero - can be from Google or other places --> 

In face of the difficulty in creating market research databases that are rich enough to allow for market segmentation and proper course-market alignment, the objective of this study is therefore to demonstrate a method for streamlining market research survey data using a combination of NLP of information obtained from social networks as well as individual subject data enrichment associated with publicly available data

## Methods

<!-- three possibilities:
1. pagina separada: survemonkey embedded + link fb app
2. pagina surveymonkey + embedded fb app
3. fb + embedded surveymonkey
 -->

Our study is split into three main sections: (1) we describe how our market research data was captured through a regular survey mechanism while enriching it through the use of Facebook application, (2) the resulting ip addresses captured through the survey are then converted to geolocation data and used for enrichment through ecological data, and (3) socio-demographic information obtained from Facebook is then used to enrich the market research data through matching with publicly available information.  

![](./study_workflow.svg)
<!-- add overview here with workflow in dot format--> 
<!-- bring to Rgraphviz -->

### Primary market research survey data and enrichment through Facebook data

<!-- user http://goo.gl/pBo9S - aqui e que esta a maior parte das informacoes interessantes
post http://goo.gl/1XEuk - isso daqui so pode ser processado com muita NLP, mas e a mina das atividades da pessoa. aqui teria de dar uma olhada nos dados pra ver o que vem mas potencialmente poderia combinar com a friendlist pra ver quem sao as pessoas que mais interagem com a pessoa pra criar uma weighted rede, ou seja, se no ultimo mes eu like ou comentei nos posts do jose eduardo um numero x de vezes esse e o peso da minha relacao com ele
friendlist http://goo.gl/BRFgy - isso daqui seria interessante mais tarde pra montar uma rede social da pessoa -->

#### Facebook application
http://goo.gl/b4Gf8
http://goo.gl/Pt4TV
http://www.quandl.com/
http://www.asdfree.com/

#### Natural language processing
<!-- NLTK -->
<!-- Jose Eduardo, please keep adding content here so that i can expand -->





### Enrichment through ip address-based geolocation data and matched ecological data

<!-- validation using existing data with physical addresses -->
<!-- Jacson, can you add some pointers so that i can expand -->
GeoIP library python
(geolitecity database](http://dev.maxmind.com/geoip/legacy/geolite)
[ggmap](http://cran.r-project.org/web/packages/ggmap/index.html)
(RSPython)[http://www.omegahat.org/RSPython/]

Inicialmente rodei o script em Python para fazer o enriquecimento dos dados geográficos a partir do número IP. 
Gerando assim um novo atributo chamado GeoIP, contendo informações de localização. A base de dados utilizada como 
fonte de informações geográficas foi obtida pelo site http://dev.maxmind.com/geoip/legacy/geolite/

```python

import pymongo
import pygeoip

from pymongo import MongoClient
client = MongoClient( "192.168.2.29")
db = client.pskills
psk = db.dados  # Collection

gi = pygeoip.GeoIP('/Users/Administrator/boxfile/GeoLiteCity.dat',pygeoip.STANDARD)

for ips in psk.find():
    print ips["IP Address"]
    geoIP = gi.record_by_addr(ips["IP Address"])
    id = { '_id' : ips["_id"]  }
    psk.update( id ,  {"$set": {"GeoIP" : geoIP} }  )

print "*** FIM "

```

Após o enriquecimento dos dados, é necessário rodar o script em R para a geração dos gráficos.


```{r}


#######################################################################################
# Example Using R with GeoIP
#######################################################################################

#####################################################################################
#SETTING ENVIRONMENT
#####################################################################################
rm(list = ls())
ls()

library("RMongo")
library(geoPlot)
library(ggplot2)
library(ggmap)
library(googleVis)
library(rjson)

#library(plyr)
#library(rworldmap)

#####################################################################################
#IMPORTING DATA AND RECODING
#####################################################################################

######### Connection MONGO DB

# mongo <- mongo.create(host="192.168.2.29", db="pskills", timeout=0L)
#query <- dbGetQuery( mongo , "dados" , "{'IP Address': '71.218.227.188' }")
#query <- dbGetQuery( mongo , "dados" , "{}", skip=0, limit=5)

mongo <- mongoDbConnect ("Trial" , "paulo.mongohq.com:10015")
username = "trial"
password = "trial"
authenticated <- dbAuthenticate(mongo, username, password)

query <- dbGetQuery( mongo , "dados" , "{}")

str(query)

query.row = nrow(query)

geoLoc <- data.frame(zip=c(1:query.row) , city=c(1:query.row) , ip=c(1:query.row),   lat=c(1:query.row),long=c(1:query.row)  , group = c(1:query.row))

for ( i in 1:1:query.row)
{
  geoTmp <- fromJSON(query$GeoIP[i])
  
  geoLoc$city[i]  <- geoTmp$city
  geoLoc$zip[i]  <- geoTmp$postal_code
  geoLoc$ip[i]   <- query$IP.Address
  geoLoc$lat[i]  <- geoTmp$latitude
  geoLoc$long[i] <- geoTmp$longitude
  geoLoc$group[i] <- 1
  
}

map <- get_map(location = 'EUA', zoom = 4)

ggmap(map) + geom_point( aes(x=long, y=lat), colour = I("red"), size = 5 , data = geoLoc, alpha = .5)

```


### Enrichment through external databases matched by sociodemographic data
<!-- Ricardo to describe matching -->

#### Public data sources
* http://www.asdfree.com/
Custom google search
* google refine extensions: https://github.com/OpenRefine/OpenRefine/wiki/Extensions
* mongodb - tuts videos + kindle books


### Schemaless database
Mongodb Schemaless and advantages in relation to lack of structure and processing power
JSON from APIs (MongoDB Manual - http://goo.gl/j7czr) <!-- Elias please add references to how mongodb imports json --> 
RDF to JSON/BSON (Stores RDF-JSON documents in MongoDB - http://goo.gl/O65GyG, Experimenting with MongoDB as an RDF Store and MongoDB-RDF) <!-- Elias, please add references on how to import RDF into mongobd --> 

### Hadoop and data distribution
<!-- Jose Eduardo and Jacson, do you think we are going to need this? --> 


### Data access by analytical tools
<!-- clients with range of template queries and connection to Python/R combination Rpy2 --> 


## Results

### Database structure
present links
<!-- Elias, please check the best way to graphically represent mongodb links --> 

### Crowdsourced data collection
<!-- make this part reproducible: pack concerto forms and post under figshare --> 


### Public data sources
<!-- compile all sources under a custom google search and present simple and embedded link --> 
<!-- present group of sites include along with parameters --> 

### Data enrichment
<!-- graphics with benchmark data tweaking parameters - make this reproducible by presenting both files with configuration as well as the final data set --> 


### Hadoop and data distribution
<!-- need to play with a three nodes on EC2 and hadoop to enhance mongodb processing power --> 
<!-- Elias, need to find way to represent parallel structure under EC2 with hadoop --> 

### Data access by analytical tools
<!-- Elias, need to check existing mongodb clients, preferably through a terminal, so that we can template queries --> 

## Discussion

### Primacy and summary

<!-- Elias, need to review the literature for each one of the headings in the methods/results in the context of data aggregation --> 

### Result 1 
### Result 2
### Result 3
### Result 4

Despite a significant process improvement in the data enrichment for market research surveys, our research has limitations. First, although the workflow is well defined, our execution is still largely manual. For example, the process to find an external database, investigate whether its content might appropriate to enrich our primary market research data, field alignment, and data merge are all done with a high degree of human interaction. Second, despite our use of state of the art natural language processing technology, the linking process is still largely iterative and error prone.

In response to the challenges described above, future research should focus on both a tighter connection among the steps described in our workflow or an optimization of this same workflow. In addition, further standardization in public data repositories should focus on the use of taxonomies that might facilitate the integration across different data sets. Despite the need for these future improvement, we do believe that the technology is ready for spread across the market research industry, with significant potential gains in process effectivenes and the corresponding financial counterpart.

## References

Jordan, Katy. (2013). MOOC completion rates. Retrieved from: http://www.katyjordan.com/MOOCproject.html. 

McAuley, A., Stewart, B., Siemens, G. and Cormier, D. (2010). “The MOOC Model 
for Digital Practice.” Retrieved from: http://www.elearnspace.org/Articles/MOOC_Final.pdf.

Pappano, L. (2012). “The Year of the MOOC.” Retrieved from: http://www.nytimes.com/2012/11/04/education/edlife/massive-open-online-courses-are-multiplying-at-a-rapid-pace.html?pagewanted=all 
