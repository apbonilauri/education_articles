# Casos de avaliação de políticas de saúde sob a perspectiva dos princípios do Sistema Único de Saúde



[prompt]
    <h2>Primeiro caso</h2>

    <p>Maria, 28 anos, vem até uma unidade de atendimento à saúde para ser atendida em relação ao seu filho de 4 anos, Marcos. Marcos tem um diagnóstico de paralisia cerebral tetraplégica, e atualmente passa parte do dia em uma escola especial. Maria relata ao médico que seu marido a abandonou em função do trabalho associado ao tratamento de Marcos. Os outros dois filhos de Maria também estão tendo problemas na escola, o mais velho aparentemente iniciando o uso de drogas. O médico interrompe Maria para dizer-lhe que os problemas familiares são algo que ela terá de resolver por si própria, já que o objetivo da consulta e a avaliação de Marcos.
    </p>

    <p>
    Analise o caso acima sob o princípio da integralidade usando um máximo de 300 palavras, identificando causas e possíveis medidas para abordar o problema. Use argumentos convincentes oriundos da sua própria experiência, observações e/ou leituras.
    </p>
[prompt]
<!-- 1. Maria, 28 anos, vem até uma unidade de atendimento à saúde para ser atendida em relação ao seu filho de 4 anos, Marcos. Marcos tem um diagnóstico de paralisia cerebral tetraplégica, e atualmente passa parte do dia em uma escola especial. Maria relata ao médico que seu marido a abandonou em função do trabalho associado ao tratamento de Marcos. Os outros dois filhos de Maria também estão tendo problemas na escola, o mais velho aparentemente iniciando o uso de drogas. O médico interrompe Maria para dizer-lhe que os problemas familiares são algo que ela terá de resolver por si própria, já que o objetivo da consulta e a avaliação de Marcos.

Analise o caso acima sob o princípio da integralidade usando um máximo de 300 palavras, identificando causas e possíveis medidas para abordar o problema. Use argumentos convincentes oriundos da sua própria experiência, observações e/ou leituras. -->



2. O Sr. Joao apresenta uma insuficiência cardíaca grave, a qual atualmente limita de maneira significativa a sua habilidade para deambular. Recentemente ele foi internado em uma Unidade de Terapia Intensiva (UTI) por haver descompensado o seu quadro clínico logo após uma infecção no braço secundária a vacinação contra pneumonia. Tendo obtido alta, o Sr. Joao começa a ter alguns novos sintomas e tenta obter uma consulta de acompanhamento junto ao cardiologista do SUS. No entanto, a agenda se encontra lotada já que vários pacientes com hipertensão com baixo risco. Quando o Sr. Joao argumenta que ele tem uma doença séria que precisa de atendimento imediato, a secretaria do ambulatório diz que ele não deve se considerar mais importante do que os outros.

Analise o caso acima sob o princípio da equidade usando um máximo de 300 palavras, identificando causas e possíveis medidas para abordar o problema. Use argumentos convincentes oriundos da sua própria experiência, observações e/ou leituras.


3. Um paciente de classe média alta chega a uma unidade de emergência com sintomas agudos de asma durante a madrugada. Sua família esta nitidamente preocupada, já que ele nunca teve uma crise tão acentuada. Ao se apresentar na portaria, a secretária avisa que ele terá de ser atendido via uma consulta privada já que ele tem condições de pagar e não tem um plano de assistência a saúde.  

Analise o caso acima sob o princípio da universalidade usando um máximo de 300 palavras, identificando causas e possíveis medidas para abordar o problema. Use argumentos convincentes oriundos da sua própria experiência, observações e/ou leituras.


4. Um paciente se apresenta para uma consulta odontológica com queixas de dor com alimentos gelados e halitose. Durante o exame, o dentista faz um diagnostico de carie dentaria e indica uma extração já que o dano e extenso. No mesmo exame e evidente uma gengivite significativa, que aparentemente tem relação com uma prática de escovação dentária inconstante. Quando o procedimento de extração dentária é concluído, o paciente é enviado a sua casa com sugestões em relação ao tratamento da dor pelo procedimento, mas nenhum aconselhamento em relação a prevenção. 

Analise o caso acima sob o conceito do empoderamento usando um máximo de 300 palavras, identificando causas e possíveis medidas para abordar o problema. Use argumentos convincentes oriundos da sua própria experiência, observações e/ou leituras.


5. Um grupo de gestão de saúde é formado para avaliar a situação de uma regional de saúde no estado do Paraná. Nessa regional é verificado que a proporção de equipes de saúde para habitantes varia de 1:1000 em áreas de população mais densa, enquanto que em zonas mais pobres e com baixa densidade esse número chega a 1:8000. Além disso, a distância para chegar até uma unidade de saúde nas regiões mais desprovidas pode ser de mais de 100 quilômetros de estradas em péssimas condições, com visitas da equipe de saúde a cada uma das famílias sendo raras. 

Analise o caso acima sob o conceito da Estratégia da Saúde da Família usando um máximo de 300 palavras, identificando causas e possíveis medidas para abordar o problema. Use argumentos convincentes oriundos da sua própria experiência, observações e/ou leituras.



6. Um grupo de obstetras reune-se com membros da secretaria de saúde para traçar medidas de atendimento a gestante dentro da regional de saúde. Durante a reunião fica claro que pacientes em regiões mais densamente povoadas tem um acompanhamento apropriado em relação a hipertensão da gravidez, mas que na áreas mais pobres e menos povoadas a taxa de eclampsia é significativamente mais alta, com óbvias consequências em relação a morbimortalidade materno infantil. 

Analise o caso acima sob o conceito da estratificação de risco usando um máximo de 300 palavras, identificando causas e possíveis medidas para abordar o problema. Use argumentos convincentes oriundos da sua própria experiência, observações e/ou leituras.


7. Um grupo de gestão de saúde se reúne para avaliar as condições da sua regional de saúde. Durante o mapeamento de doenças e estratégias de prevenção, fica óbvio que na cidade sede do município as condições da população estão em um nível aceitável. No entanto, na região noroeste da regional de saúde existe um foco onde as coletas de preventivos são bastante abaixo da média. O grupo imediatamente inicia uma discussão a respeito de o governo federal ser inteiramente responsável por essa situação já que recursos não foram destinados em quantidade suficiente para que houvesse uma distribuição homogênea.

Analise o caso acima sob o conceito da territorialização em saúde usando um máximo de 300 palavras, identificando causas e possíveis medidas para abordar o problema. Use argumentos convincentes oriundos da sua própria experiência, observações e/ou leituras.

8. Um paciente chega ao pronto atendimento com dor precordial intensa. Um ECG e enzimas confirmam um diagnóstico de infarto agudo do miocárdio. Como esse é um hospital de pequeno porte, após prestar os primeiros atendimento e sem condições de dar um atendimento adequado a esse paciente, o médico de plantão imediatamente inicia uma série de ligações telefônicas tentando conseguir uma vaga em hospitais com condições para atender esse caso. Depois de tentativas com os cinco hospitais da região que poderiam atender esse paciente, nenhum tem possibilidades de atendimento e o paciente é enviado a capital do estado que fica a 300 quilômetros de distancia com uma carta explicando o problema, identificando causas e possíveis medidas para abordar o problema.


Analise o caso acima sob o conceito de redes de atenção usando um máximo de 300 palavras, identificando causas e possíveis medidas para abordar o problema. Use argumentos convincentes oriundos da sua própria experiência, observações e/ou leituras.


9. Um governo estadual lança uma campanha para destinar recursos substanciais ao combate a fome, já que esse é um problema que ainda assola uma porcentagem da sua população. Na capital do estado, no entanto, as taxas de diabetes e obesidade apresentam uma taxa de crescimento alta, e o governo do município argumenta que essas doenças deveriam ter prioridade quando comparada com o combate a fome naquela região.O governo estadual sustenta que a capital não deve ter prioridades paralelas, já que isso não criaria uma boa imagem publica de alinhamento naquele estado. O gestor de saúde da regional, por sua vez, acredita que tem autonomia para decidir como definir as estratégias e metas em saúde e como os recursos serão alocados.


Analise o caso acima sob o conceito de descentralização usando um máximo de 300 palavras, identificando causas e possíveis medidas para abordar o problema. Use argumentos convincentes oriundos da sua própria experiência, observações e/ou leituras.


10. Verbas estaduais e municipais são destinadas a um novo programa de monitoramento da pressão arterial em um pólo do estado com altas taxas de acidente vascular cerebral e infarto agudo do miocárdio. Apesar de uma grande promoção por jornais e radio, em poucos meses fica obvio que os recursos serão focados apenas nos polos mais populosos da região, deixando de lado as localizações onde a população mais necessitava de ajuda. Quando jornalistas entrevistam lideres comunitários, a opinião geral e que eles estão a mercê dos interesses políticos do governo, e que nada pode ser feito para melhorar essa situação, a resignação sendo a única alternativa. 

Analise o caso acima sob o conceito de participação social usando um máximo de 300 palavras, identificando causas e possíveis medidas para abordar o problema. Use argumentos convincentes oriundos da sua própria experiência, observações e/ou leituras.


** Guias para correção **
[rubric]
+ Correção
- 0 = Não fala do conceito da questão
- 1 = Fala do conceito de forma simplificada e superficial e não contextualiza a resposta com o caso
- 2 = Fala do conceito de forma simplificada e superficial e contextualiza a resposta com o caso
- 2 = Fala do conceito de forma completa e não contextualiza a resposta com o caso
- 3 = Fala do conceito de forma completa e contextualiza a resposta com o caso
- 4 = Fala do conceito de forma completa, contextualiza a resposta com o caso e articula com outros conceitos/princípios do  (Essa pode ser excluida no fina - caso sejam apenas 4 pontos de soma)
[rubric]

<!-- 0 = Não fala do conceito da questão
1 = Fala do conceito de forma simplificada e superficial e não contextualiza a resposta com o caso
2 = Fala do conceito de forma simplificada e superficial e contextualiza a resposta com o caso
2 = Fala do conceito de forma completa e não contextualiza a resposta com o caso
3 = Fala do conceito de forma completa e contextualiza a resposta com o caso
4 = Fala do conceito de forma completa, contextualiza a resposta com o caso e articula com outros conceitos/princípios do  (Essa pode ser excluida no fina - caso sejam apenas 4 pontos de soma)
 -->
