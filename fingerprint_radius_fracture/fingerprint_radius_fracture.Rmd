# Inter and Intra-Observer Agreement of Distal Radius Fractures Before and After the Exposure to AO Classification with or without an expert fingerprint: A reproducible research study

<!-- make sure to include embodied cognition (emotions) within situated schema
cito for references -->

Fabiano Caumo  
Pedro Gaspar Soares Justo  
Henrique Ayzemberg  
Joao Ricardo Vissoci  
Ricardo Pietrobon  
Ana Paula Bonilauri Ferreira  

<!-- add sporedata como affiliation
conferences CBO
 -->

## Abstract

## Introduction
The ability to move surgical resident knowledge from a novice to an expert level has implications at multiple levels. First, it has obvious implications for the future healthcare workforce, directly impacting on the quality of care which will be provided to patients in a few years. 
<!-- refs on workforce predictions, including Ricardo's paper -->
Second, it has immediate consequences in relation to the quality of care provided to patients under the direct care of in-training residents. Given that residents are in the process of learning, the better the quality of their training, the lower the probability of errors while they care for patients. Despite the extreme significance of the novice-expert transition in resident education, to our knowledge no previous studies have addressed the technology gap relating the novice-expert transition.

<!-- novice expert transition literature - check kindle 
http://goo.gl/xoXiI
-->
<!-- meta-cognition literature -->


The objective of this study was therefore to investigate the exposure of residents to a novel immersive training based on expert fingerprints, specifically focusing on its impact on the intraobserver and interobserver reliability of the AO classification of distal radius fractures.

<!-- reproducible methodology, insert paper joao arxiv -->

## Methods

### Institutional Review Board

We obtained approval from the Research Ethics Committee of São José Municipal Hospital  prior to the initiation of this project.  All study participants provided informed consent prior to enrollment in the study.

### Subjects

orthopedic residents (R2 e R3)

### Radiographic images

at least 10 images, with fracture pattern variation.
 Procedures

### First assessment
pre-training 

present a collection of five images.
each participant independently classifies the images according to the AO Classification. Afterwards, the answers are collected.
a discussion about the different  opinions will be coordinated to establish consensus. This will be eased by a detailed explanation of  the classifications.
five new images will be presented and classified independently by each participant.
go back to step “3”.
document each image, discussions, and progressive modifications to the scale.
present 20 new images (different from those presented in pre-test training). The participants classify the images according to the AO Classification System.
During the 30 days interval, the "expert" will be interviewed to explain how he thinks while classifies a fracture image. The AO Classification figures of different patterns of distal radius fractures will be shown to him. The interview will be recorded to allow represent their cognitive schemata. 
repeat the presentation of the same 20 images in 30 days. However, before presenting the images, a "tutorial" video about how the expert thinks to classify a fracture image according to AO Classification System will be projected.

### Cognitive fingerprints


<!-- # Generating items from situated schemata



## Definitions

1. situated schema: set of concepts and situations
1. situated schema mapping:- the graphical representation of a situated schema, usually represented by a matrix or its corresponding social network, visually represented by a software like Gephi
1. concept: any word 
1. situation: image, narrative with emotion, video, tools and corresponding tricks of the trade
1. stem: question with concept or situation equivalent in an item
1. option: the alternative options to an item, or their situation equivalents
1. concept or situation hierarchy - any concept or situation that is at the same level, be it a diagnosis, biomechanics, or therapeutic plan







## Item creation based on situated schema mapping




### concept -> situation

#### Item example: A1.2 -> corresponding image
    * Item stem: "What is the image that corresponds to this classification?"
    * Options are different images where only one is the correct one

#### Item example: invert the sequence
* corresponding image -> A1.2
* Item stem: "What is the classification that corresponds to this image?"
* Options are different types of classification




### two hierarchically contiguous concepts 

#### diagnostic concept -> biomechanics
* Item example: A1.2 -> undisplaced reduceable transverse fracture
* Item stem: "Which biomechanical considerations should you make once you have a A1.2 diagnosis?"
* Options
    * one biomechanical concept or situation connected to the diagnosis and all other biomechanical concepts or situations not connected to that diagnosis
    * summed items: several options are correct and the answer to the item is the sum of all correct answers (options = 1, 2, 4, 8, 16, 32, 64, ...)

#### invert sequence: biomechanics -> diagnostic concept
* Item stem: "Having an undisplaced reduceable transverse fracture of the distal third of the radius is an important biomechanical consideration when you are considering what kind of diagnosis?"
* replace concept by situation





### two hierarchically non-contiguous concepts

#### infer the intermediate concept
* A1.2 -> protective plate
* Item stem: "For a A1.2 fracture, what would be the biomechanical requirements for you to indicate therapy with a protective plate?"
* Options: single or summed option
* swap concept by situation

#### indicate the non-contiguous concept
* A1.2 -> protective plate
* Item stem: "What are the possible types of fracture that can be treated with a protective plate?"



### Choice of non-correct options
Non correct options can be chosen from any concept or situation that is within the same hierarchy



## Rule for non-automatic item generation (AIG)

* Create at least five items per diagnosis
* Within these five try to alternate among the following rules
    * concept -> situation
    * hierarchically contiguous concepts
    * hierarchically non-contiguous concepts
 -->


 
### Automatic item generation

1. classification/x-ray figure OR CT
1. biomechanics/detailed fracture figure OR drawing with actual mechanism OR video
1. surgery/device picture OR video

# definitions
classification -> category
biomechanics -> category
surgery -> category
x-ray -> figure
fracturefig -> figure
device -> figure


#items -- all can be reversed
category -> image
permutation of each category/image
classification -> biomechanics
biomechanics -> surgery
classification -> surgery



#### Expert interviews

#### Training-assessment preparation
In order to prepare the training for each student, we evaluated each of the fingerprints looking for the following factors: (1) the traditional sequence of diagnosis, evaluation, and therapeutic plan, (2) the hubs or nodes with the largest number of edges, (3) alternative paths leading from the diagnosis to the therapy. 
<!-- later on add examples for each of these three -->



### Expert analysis
An interview with an expert was conducted where he was presented with a series of radiographs with different types of distal radius fractures. The subsequent evaluation proceeded then in three consecutive sessions. First, he was asked to "think aloud" about his thoughts on what he was seeing, a potential AO classification for that fracture, what constituted the underlying biomechanics and potentially associated conditions, and finally the therapeutic plan. On a second session, the same expert was shown a AO table with classification, fracture characteristics and therapeutic plan. The expert was then asked to comment on how his conduct differed from those. Finally, during the third session the expert was presented with a series of "handicapped cases," where individual radiographs had certain areas covered in order to verify

   
### Statistical Analysis

dados da matriz será a variabilidade do das fingerprints dos expertise
Avaliar as importancia ou o grau das relação em uma VAS - Visual Analogue Scale
Results

## Results



```{r}
setwd("/Users/rpietro/ps_articles/fingerprint_radius_fracture/")
require(igraph) # This loads the igraph package
# dataset at http://goo.gl/0s7tP
ao=read.csv("AO.csv",header=TRUE,row.names=1,check.names=FALSE) # choose an adjacency matrix from a .csv file
m=as.matrix(ao) # coerces the data set as a matrix
g=graph.adjacency(m,mode="undirected",weighted=NULL, add.colnames=NA) # this will create an 'igraph object'
plot.igraph(g)
```

[Network Visualization of Key Driver Analysis](http://goo.gl/Rcsn4 )

## Discussion
http://goo.gl/NwMNN
http://goo.gl/17QP

Uniqueness and summary of main findings


Discussion of the each of the main findings

Authors agreeing and discussion on mechanisms underlying the finding
Authors disagreeing with findings and reasons for disagreement


Study limitations



Conclusions

First, provide readers with a description of future studies that should be conducted to further expand the field.
Point readers in relation to how the information presented in this manuscript might make a difference in practice.  In other words, how can the original information you have just generated be transformed into knowledge

<!-- (próximo estudo: RCT com metade alocado pra sem schema e outra metade com schema, mas vamos deixar isso pra depois caso a gente realmente veja uma melhora do agreement)
 -->


References
(autores, título, revista, ano, mês, número, volume, página)
