# Inter and Intra-Observer Agreement of Distal Radius Fractures Before and After the Exposure to AO Classification with or without an expert fingerprint: A reproducible research study


Fabiano Caumo  
Pedro Gaspar Soares Justo  
Henrique Ayzemberg  
Joao Ricardo Vissoci  
Ana Paula Bonilauri Ferreira  
Ricardo Pietrobon  

## Abstract

## Introduction
The ability to move surgical resident knowledge from a novice to an expert level has implications at multiple levels. First, it has obvious implications for the future healthcare workforce, directly impacting on the quality of care which will be provided to patients in a few years. 
<!-- refs on workforce predictions, including Ricardo's paper -->
Second, it has immediate consequences in relation to the quality of care provided to patients under the direct care of in-training residents. Given that residents are in the process of learning, the better the quality of their training, the lower the probability of errors while they care for patients. Despite the extreme significance of the novice-expert transition in resident education, to our knowledge no previous studies have addressed the technology gap relating the novice-expert transition.


<!-- novice expert transition literature - check kindle 
http://goo.gl/xoXiI
-->
<!-- meta-cognition literature -->


The objective of this study was therefore to investigate the exposure of residents to a novel immersive training based on expert fingerprints, specifically focusing on its impact on the intraobserver and interobserver reliability of the AO classification of distal radius fractures.


## Methods

### Institutional Review Board

We obtained approval from the Research Ethics Committee of São José Municipal Hospital  prior to the initiation of this project.  All study participants provided informed consent prior to enrollment in the study.

### Subjects

orthopedic residents (R2 e R3)

### Radiographic images

at least 10 images, with fracture pattern variation.
 Procedures

### First assessment
pre-training 

present a collection of five images.
each participant independently classifies the images according to the AO Classification. Afterwards, the answers are collected.
a discussion about the different  opinions will be coordinated to establish consensus. This will be eased by a detailed explanation of  the classifications.
five new images will be presented and classified independently by each participant.
go back to step “3”.
document each image, discussions, and progressive modifications to the scale.
present 20 new images (different from those presented in pre-test training). The participants classify the images according to the AO Classification System.
During the 30 days interval, the "expert" will be interviewed to explain how he thinks while classifies a fracture image. The AO Classification figures of different patterns of distal radius fractures will be shown to him. The interview will be recorded to allow represent their cognitive schemata. 
repeat the presentation of the same 20 images in 30 days. However, before presenting the images, a "tutorial" video about how the expert thinks to classify a fracture image according to AO Classification System will be projected.

### Cognitive fingerprints


### Automatic item generation

1. classification/x-ray figure OR CT
1. biomechanics/detailed fracture figure OR drawing with actual mechanism OR video
1. surgery/device picture OR video

# definitions
classification -> category
biomechanics -> category
surgery -> category
x-ray -> figure
fracturefig -> figure
device -> figure


#items -- all can be reversed
category -> image
permutation of each category/image
classification -> biomechanics
biomechanics -> surgery
classification -> surgery



#### Expert interviews

#### Training-assessment preparation
In order to prepare the training for each student, we evaluated each of the fingerprints looking for the following factors: (1) the traditional sequence of diagnosis, evaluation, and therapeutic plan, (2) the hubs or nodes with the largest number of edges, (3) alternative paths leading from the diagnosis to the therapy. 
<!-- later on add examples for each of these three -->



### Expert analysis
An interview with an expert was conducted where he was presented with a series of radiographs with different types of distal radius fractures. The subsequent evaluation proceeded then in three consecutive sessions. First, he was asked to "think aloud" about his thoughts on what he was seeing, a potential AO classification for that fracture, what constituted the underlying biomechanics and potentially associated conditions, and finally the therapeutic plan. On a second session, the same expert was shown a AO table with classification, fracture characteristics and therapeutic plan. The expert was then asked to comment on how his conduct differed from those. Finally, during the third session the expert was presented with a series of "handicapped cases," where individual radiographs had certain areas covered in order to verify

   
### Statistical Analysis

dados da matriz será a variabilidade do das fingerprints dos expertise
Avaliar as importancia ou o grau das relação em uma VAS - Visual Analogue Scale
Results

## Results




```r
setwd("/Users/rpietro/ps_articles/fingerprint_radius_fracture/")
require(igraph)  # This loads the igraph package
```

```
## Loading required package: igraph
```

```
## Warning: there is no package called 'igraph'
```

```r
# dataset at http://goo.gl/0s7tP
ao = read.csv("AO.csv", header = TRUE, row.names = 1, check.names = FALSE)  # choose an adjacency matrix from a .csv file
m = as.matrix(ao)  # coerces the data set as a matrix
g = graph.adjacency(m, mode = "undirected", weighted = NULL, add.colnames = NA)  # this will create an 'igraph object'
```

```
## Error: could not find function "graph.adjacency"
```

```r
plot.igraph(g)
```

```
## Error: could not find function "plot.igraph"
```


[Network Visualization of Key Driver Analysis](http://goo.gl/Rcsn4 )

## Discussion
http://goo.gl/NwMNN
http://goo.gl/17QP

Uniqueness and summary of main findings


Discussion of the each of the main findings

Authors agreeing and discussion on mechanisms underlying the finding
Authors disagreeing with findings and reasons for disagreement


Study limitations



Conclusions

First, provide readers with a description of future studies that should be conducted to further expand the field.
Point readers in relation to how the information presented in this manuscript might make a difference in practice.  In other words, how can the original information you have just generated be transformed into knowledge

<!-- (próximo estudo: RCT com metade alocado pra sem schema e outra metade com schema, mas vamos deixar isso pra depois caso a gente realmente veja uma melhora do agreement)
 -->


References
(autores, título, revista, ano, mês, número, volume, página)
