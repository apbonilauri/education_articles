setwd("~/Desktop")

# setting seed for reproducible results
set.seed(666)

# creating categorical variables
MF  <- c("female", "male") #creating categories and storing in the MF object
gender <- sample( MF[1:2], 10000, replace=TRUE, prob=c(0.5, 0.5)) #generating 1000 observations using a 50:50 proportion 
prop.table(table(gender)) #display final variable in a table format

age_cat  <- c("0-10", "10-20", "20-30", "30-40")
age <- sample( age_cat[1:4], 10000, replace=TRUE, prob=c(0.1, 0.3, 0.4, 0.2) )
prop.table(table(age))

income  <- rnorm(n=1000, m=20000, sd=5000) # generating 1000 observations with a mean of 20000 dollars and standard deviation of 5000
hist(income) # histogram

simulated_dataset  <- data.frame(gender, age, income) # joining all vectors into a single data frame (spreadsheet-like format)
head (simulated_dataset) # checking the first few observations

write.csv(simulated_dataset, file = "data.csv")